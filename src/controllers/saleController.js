const controllerSales = {};
var queries = [
    "SELECT * FROM producto",
    "SELECT * FROM tipoproducto",
    "SELECT * FROM magnitud",
    "SELECT * FROM tipoproducto where id_tipoProducto=?"
    ];

controllerSales.vender =  (req, res) => {
    req.getConnection((error, conn) =>{
        //"SELECT v.id_Venta,v.fecha_Venta,u.nombre,sum(dv.cantidad*p.precio) as 'Total' FROM venta v join usuario u on v.id_Usuario=u.id_Usuario join detalle_venta dv on dv.id_Venta=v.id_Venta join producto p on p.id_producto=dv.id_producto join tipoproducto tpro on tpro.id_tipo=p.id_tipoProducto group by(v.id_Venta)"
        conn.query('SELECT * from venta', (err, vent) =>{
                
            conn.query(queries[0], (err, prod) =>{
                
                    conn.query(queries[2],(err, tip) =>{
                        console.log(prod)
                        
                        res.render('sales', {
                            
                            data : vent,
                            categoria : tip,
                            producto : prod
                             
                            
                        });
                    });
            });
        });
    });
};
controllerSales.hist =  (req, res) => {
    req.getConnection((error, conn) =>{
        //"SELECT v.id_Venta,v.fecha_Venta,u.nombre,sum(dv.cantidad*p.precio) as 'Total' FROM venta v join usuario u on v.id_Usuario=u.id_Usuario join detalle_venta dv on dv.id_Venta=v.id_Venta join producto p on p.id_producto=dv.id_producto join tipoproducto tpro on tpro.id_tipo=p.id_tipoProducto group by(v.id_Venta)"
        conn.query(`SELECT v.id_Venta,v.fecha_Venta,u.nombre,c.id_cliente_cedula,sum(dv.cantidad*p.precio) as 'Total' 
        FROM venta v join usuario u on v.id_Usuario=u.id_Usuario 
        join cliente c on c.id_cliente_cedula=v.id_Cliente
        join detalle_venta dv on dv.id_Venta=v.id_Venta
        join producto p on p.id_Producto=dv.id_Producto 
        join tipoproducto tpro on tpro.id_tipoProducto=p.id_TipoProducto group by(v.id_Venta)`, (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                console.log(rows);
                res.render('sales_hist', {
                    
                    data: rows,
                })
            }
        })
    })
};

controllerSales.his =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM usuario where activo=0', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('users_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerSales.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO venta set ?', [data], (err, rows) =>{
            res.redirect('/sales');
        })
    })
}

controllerSales.edit = (req, res) =>{
    const {id} = req.params;
    req.getConnection((err, conn) =>{
        conn.query('SELECT * FROM venta WHERE id_Venta = ?', [id], (err, rows) =>{
            
            res.render('sales_edit', {
                data: rows[0]
            });
        });
    });
}

controllerSales.update = (req, res) =>{
    const {id} = req.params;
    const newData = req.body;
    req.getConnection((err, conn) =>{
        conn.query('UPDATE venta set ? WHERE id_Venta = ?', [newData, id], (err, rows) =>{
            res.redirect('/sales');
        })
    });
}

controllerSales.delete = (req, res) => {
    req.getConnection((err, conn) => {
        const {id} = req.params;
        conn.query('DELETE FROM venta WHERE id_Venta = ?', [id], (err, rows) =>{
            res.redirect('/sales');
        })
    })
}

controllerSales.deudasHis =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM deuda', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('deudas_hist', {
                    data: rows
                })
            }
        })
    })
};

module.exports = controllerSales;