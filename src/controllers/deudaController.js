const controllerDeudas = {};

controllerDeudas.list =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM deuda', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('deudas', {
                    data: rows
                })
            }
        })
    })
};

controllerDeudas.deudasHis =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM deuda', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('deudas_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerDeudas.his =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM usuario where activo=0', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('users_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerDeudas.save = (req, res) => {
    const data = req.body;
    //console.log(data);
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO deuda set ?', [data], (err, rows) =>{
            //console.log(rows);
            res.redirect('/deudas');
        })
    })
}

controllerDeudas.edit = (req, res) =>{
    const {id} = req.params;
    req.getConnection((err, conn) =>{
        conn.query('SELECT * FROM deuda WHERE id_Deuda = ?', [id], (err, rows) =>{
            
            res.render('deudas_edit', {
                data: rows[0]
            });
        });
    });
}

controllerDeudas.update = (req, res) =>{
    const {id} = req.params;
    const newData = req.body;
    req.getConnection((err, conn) =>{
        conn.query('UPDATE usuario set ? WHERE id_Usuario = ?', [newData, id], (err, rows) =>{
            res.redirect('/deudas');
        })
    });
}

controllerDeudas.delete = (req, res) => {
    req.getConnection((err, conn) => {
        const {id} = req.params;
        conn.query('DELETE FROM deuda WHERE id_Deuda = ?', [id], (err, rows) =>{
            res.redirect('/deudas');
        })
    })
}

module.exports = controllerDeudas;