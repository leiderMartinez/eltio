const controllerProducts = {};
var queries = [
    "SELECT * FROM producto WHERE id_Producto = ?",
    "SELECT * FROM tipoproducto",
    "SELECT * FROM magnitud",
    "SELECT * FROM tipoproducto where id_tipoProducto=?"
    ];

controllerProducts.list =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT id_Producto,p.nombre,tp.descripcion as categoria,m.abreviatura as magnitud,precio,existencias,cant_Minima,cant_Maxima  FROM producto p join  tipoproducto tp on p.id_TipoProducto=tp.id_tipoProducto join magnitud m on m.id_Magnitud=p.id_Magnitud where descontinuado=0', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('products', {
                    data: rows
                })
            }
        })
    })
};

controllerProducts.his =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM usuario where activo=0', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('users_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerProducts.listdescon =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT id_Producto,p.nombre,tp.descripcion as categoria,m.abreviatura as magnitud,precio,existencias,cant_Minima,cant_Maxima  FROM producto p join  tipoproducto tp on p.id_TipoProducto=tp.id_tipoProducto join magnitud m on m.id_Magnitud=p.id_Magnitud where descontinuado=1', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('products_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerProducts.categ =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query(queries[1], (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('categoria', {
                    data: rows
                })
            }
        })
    })
};


controllerProducts.save = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO producto set ?', [data], (err, rows) =>{
            res.redirect('/products');
        })
    })
}

controllerProducts.edit = (req, res) =>{
    const {id} = req.params;
    req.getConnection((err, conn) =>{
        conn.query(queries[0], [id], (err, rows) =>{
            
            conn.query(queries[1], [id], (err, resul) =>{
            
                conn.query(queries[2], [id], (err, mag) =>{
            
                    res.render('products_edit', {
                        
                        data : rows[0],
                        categoria : resul,
                        magnitud : mag
                         
                        
                    });
                });
            });
        });
        
    });
}
controllerProducts.categedit = (req, res) =>{
    const {id} = req.params;
    req.getConnection((err, conn) =>{
        conn.query(queries[3], [id], (err, rows) =>{
            
            conn.query(queries[1], [id], (err, resul) =>{
            
                conn.query(queries[2], [id], (err, mag) =>{
            
                    res.render('products_edit', {
                        
                        data : rows[0],
                        categoria : resul,
                        magnitud : mag
                         
                        
                    });
                });
            });
        });
        
    });
}

controllerProducts.update = (req, res) =>{
    const {id} = req.params;
    const newData = req.body;
    req.getConnection((err, conn) =>{
        conn.query('UPDATE producto set ? WHERE id_producto = ?', [newData, id], (err, rows) =>{
            res.redirect('/products');
        })
    });
}

controllerProducts.desco = (req, res) => {
    req.getConnection((err, conn) => {
        const {id} = req.params;
        conn.query('UPDATE producto set descontinuado=1 WHERE id_Producto = ?', [id], (err, rows) =>{
            res.redirect('/products');
        })
    })
}

controllerProducts.rein = (req, res) => {
    req.getConnection((err, conn) => {
        const {id} = req.params;
        conn.query('UPDATE producto set descontinuado=0 WHERE id_Producto = ?', [id], (err, rows) =>{
            res.redirect('/products');
        })
    })
}

controllerProducts.deudasHis =  (req, res) => {
    req.getConnection((error, conn) =>{
        conn.query('SELECT * FROM deuda', (err, rows) =>{
            if (err) {
                res.json(err);
            } else {
                //console.log(rows);
                res.render('deudas_hist', {
                    data: rows
                })
            }
        })
    })
};

controllerProducts.add = (req, res) => {          
    req.getConnection((err, conn) =>{  
        conn.query(queries[1], (err, resul) =>{
        
            conn.query(queries[2], (err, mag) =>{
        
                res.render('products_add', {
                    categoria : resul,
                    magnitud : mag
                     
                    
                });
            });
        });    
    });
}

module.exports = controllerProducts;